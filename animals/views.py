from os import name, sep
from django.shortcuts import render

# Create your views here.

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import Http404
from django.http.response import JsonResponse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
import random
from . import models
import pandas as pd
import os
from animals.models import Article
from config.settings import BASE_DIR
def isascnum(s):#英数字判定
    return True if s.isdecimal() and s.isascii() else False


def title(request):
    return render(request, 'animals/title.html')

def home(request):
    return render(request, 'animals/home.html')

def dog_cat_choice(request, q_choice):
    if q_choice == 1:
        data = {
            'cat': 1,
            'dog': 4,
        }
    elif q_choice == 2:
        data = {
            'cat': 2,
            'dog': 5,
        }
    else:
        data = {
            'cat': 3,
            'dog': 6,
        }
    return render(request, 'animals/dog_cat_choice.html', data)

def quiz(request, fie, used, true, false):
    df = pd.read_csv("animals/text_file/ques.txt", sep=" ")
    df_q = df[df["fie"] == fie]
    df_q = df_q.reset_index()
    for i in range(len(df_q)):
        if used == df_q.at[i, 'ques']:
            df_q = df_q.drop([i])
    df_r = df_q.sample(frac=1).reset_index(drop=True)

    times = true + false
    data = {
        'fie': fie,
        'ques': df_r.at[times,'ques'],
        'cho1': df_r.at[times,'cho1'],
        'cho2': df_r.at[times,'cho2'],
        'cho3': df_r.at[times,'cho3'],
        'cho4': df_r.at[times,'cho4'],
        'ans': df_r.at[times,'ans'],
        'ex': df_r.at[times,'com'],
        'true': true,
        'false': false,
        'times': times + 1,
    }
    return render(request, 'animals/quiz.html', data)

def answer(request, cho, fie, used, ans, ex, true, false, old):
    if cho == ans:
        a = True
        true += 1
    else:
        a = False
        false += 1
    
    times = true + false

    data = {
        'cho': a,
        'fie': fie,
        'used': used,
        'ans': ans,
        'ex': ex,
        'times': times,
        'true': true,
        'false': false,
    }
    if old == 1:
        return render(request, 'animals/ansewr_2.html', data)
    
    return render(request, 'animals/ansewr.html', data)

def result(request, true, false, old):
    data = {
            'num_true': true,
            'num_false': false,
    }

    if old == 1:
        return render(request, 'animals/result_2.html', data)
     
    return render(request, 'animals/result.html', data)

@csrf_exempt
def make_account(request):
    return render(request, 'animals/make_account.html')

@csrf_exempt
def make_account2(request):
    a = 0
    acc = request.POST['acc']
    surname = request.POST['surname']
    name = request.POST['name']
    che = request.POST['check']
    pas = request.POST['pas']

    if pas.islower() :#全て小文字か判定
        print('全て小文字になっています')
        return render(request, 'animals/make_account.html')
    elif pas.isupper() :#全て大文字か判定
        print('全て大文字になっています')
        return render(request, 'animals/make_account.html')
    elif isascnum(pas):#全て英数字か判定
        print('小文字と大文字を使って下さい')
        return render(request, 'animals/make_account.html')
    elif pas.isalpha():#全てスペルか判断
        print('数字を使ってください')
        return render(request, 'animals/make_account.html')
    elif len(pas) < 8 :#字数判定
        print('８字以上使用して下さい')
        return render(request, 'animals/make_account.html')
    elif pas != che:#passの正確性確認
        print('パスワードと確認用パスワードが合っていません')
        return render(request, 'animals/make_account.html')

    df = pd.read_csv("animals/text_file/account.txt", sep=" ")
    for i in df['acc']:
        if acc == i:
            a += 1
            if pas == df.at[a, 'pass']:
                return render(request, 'animals/make_account.html')
    with open("animals/text_file/account.txt", 'a') as file_object:
        file_object.write(acc.title() + ' '  + surname.title() + ' ' + name.title() + ' ' + pas.title() + "\n")
    return render(request, '{% url "author" %}')
    
def log_in(request):
    return render(request, 'animals/log_in.html')

def log_in2(request):
    a = 0
    df_a = pd.read_csv("animals/text_file/account.txt", sep=" ")
    acc = request.POST['acc']
    pas = request.POST['pas']
    for i in df_a['acc']:
        if acc == i:
            a += 1
            if pas != df_a.at[a, 'pass']:
                return render(request, 'animals/log_in.html')
    return redirect('author')

@csrf_exempt
def author(request):
    df_q = pd.read_csv("animals/text_file/ques.txt", sep=" ")
    df1 = df_q[df_q["fie"] == 1]
    df1 = df1.drop('fie', axis=1)
    df2 = df_q[df_q["fie"] == 2]
    df2 = df2.drop('fie', axis=1)
    df3 = df_q[df_q["fie"] == 3]
    df3 = df3.drop('fie', axis=1)
    df4 = df_q[df_q["fie"] == 4]
    df4 = df4.drop('fie', axis=1)
    df5 = df_q[df_q["fie"] == 5]
    df5 = df5.drop('fie', axis=1)
    df6 = df_q[df_q["fie"] == 6]
    df6 = df6.drop('fie', axis=1)
    data = {
        'article': Article.objects.all(),
        'df1_to' : df1.values.tolist(),
        'df2_to' : df2.values.tolist(),
        'df3_to' : df3.values.tolist(),
        'df4_to' : df4.values.tolist(),
        'df5_to' : df5.values.tolist(),
        'df6_to' : df6.values.tolist(),
        'count' : str(len(df1)),
        'fie' : 0,
    }
    return render(request, 'animals/author.html', data)

def make_ques(request):
    if request.method == 'POST':
        fie = request.POST['fie']
        ques = request.POST['ques']
        cho1 = request.POST['cho1']
        cho2 = request.POST['cho2']
        cho3 = request.POST['cho3']
        cho4 = request.POST['cho4']
        ans = request.POST['ans']
        com = request.POST['com']
        with open('animals/text_file/ques.txt', 'a') as f:
            f.write(fie.title() + " " + ques.title()+ " " + cho1.title()+ " " + cho2.title()+ " " + cho3.title()+ " " + cho4.title()+ " " + ans.title()+ " " + com.title() + "\n")
        return redirect('author')

    return render(request, 'animals/make_ques.html')

def make_ques2(request):
    fie = request.POST['fie']
    ques = request.POST['ques']
    cho1 = request.POST['cho1']
    cho2 = request.POST['cho2']
    cho3 = request.POST['cho3']
    cho4 = request.POST['cho4']
    ans = request.POST['ans']
    com = request.POST['com']
    with open('animals/text_file/ques.txt', 'a') as f:
        f.write(fie.title() + " " + ques.title()+ " " + cho1.title()+ " " + cho2.title()+ " " + cho3.title()+ " " + cho4.title()+ " " + ans.title()+ " " + com.title() + "\n")
    return redirect('author')

def delete(request, fie, q_text):
    df_all = pd.DataFrame({'fie':[], 'ques':[], 'cho1':[], 'cho2':[], 'cho3':[], 'cho4':[], 'ans':[], 'com':[], 'like':[]})
    old_df = 'animals/text_file/ques.txt'
    df = pd.read_csv(old_df, sep=" ")
    df_q = df[df["fie"] == fie]
    df_q = df_q.reset_index()
    out_text = df_q.iloc[q_text]
    print(out_text)
    out_text = out_text[8]

    for i in range(len(df)):
        if out_text != df.at[i, 'com']:
            df_all = df_all.append(df.iloc[i])
    df_all['fie'] = df_all['fie'].astype(int)
    df_all['ans'] = df_all['ans'].astype(int)
    df_all['like'] = df_all['like'].astype(int)
    os.remove(old_df)
    df_all.to_csv(old_df, sep=' ', index=False)
    return redirect('author')

def like(request, fie, q_text):
    df_all = pd.DataFrame({'fie':[], 'ques':[], 'cho1':[], 'cho2':[], 'cho3':[], 'cho4':[], 'ans':[], 'com':[]})
    old_df = 'animals/text_file/ques.txt'
    df = pd.read_csv(old_df, sep=" ")
    df_q = df[df["fie"] == fie]
    df_q = df_q.reset_index()
    like = df_q.iloc[q_text]
    like = like[8]
    for i in range(len(df)):
        if like == df.at[i, 'com']:
            df.at[i, 'like'] += 1
            df_all = df_all.append(df.iloc[i])
        else:
            df_all = df_all.append(df.iloc[i])

    df_all['fie'] = df_all['fie'].astype(int)
    df_all['ans'] = df_all['ans'].astype(int)
    df_all['like'] = df_all['like'].astype(int)
    os.remove(old_df)
    df_all.to_csv(old_df, sep=' ', index=False)
    return redirect('author')