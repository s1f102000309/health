
from unicodedata import name
from django.urls import path
from . import views

urlpatterns = [
	path('title', views.title , name='title'),
  path('home', views.home, name='home'),
  path('dog_cat_choice/<int:q_choice>', views.dog_cat_choice, name='dog_cat_choice'),
  path('quiz/<int:fie>/<str:used>/<int:true>/<int:false>', views.quiz, name='quiz'),
  path('answer/<int:cho>/<int:fie>/<str:used>/<int:ans>/<str:ex>/<int:true>/<int:false>/<int:old>', views.answer, name='answer'),
  path('result/<int:true>/<int:false>/<int:old>', views.result, name='result'),

  path('log_in', views.log_in, name='log_in'),
  path('log_in2', views.log_in2, name='log_in2'),
  path('make_account', views.make_account, name='make_account'),
  path('make_account2', views.make_account2, name='make_account2'),
  path('author', views.author, name='author'),
  path('make_ques', views.make_ques, name='make_ques'),
  path('make_ques2', views.make_ques2, name='make_ques2'),
  path('delete/<int:fie>/<int:q_text>', views.delete, name='delete'),
  path('like/<int:fie>/<int:q_text>', views.like, name='like'),
]